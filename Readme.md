# Poly Sculptor VR

Small VR project developed by our team during NTI 2020.
All scripts and assets in our project have MIT License.

## Project structure

In our project we name assets folders with a leading underscore, to keep them on top.
Classes that used for the Interaction System are located in the `PolySculptor` namespace. 
In addition to our own scripts, we have some open-source scripts. 
We are preserving all Copyright and Licenses, so you can easily found the 
source repository of these files.

## Code conventions

We use the standard naming convention that described
[here](https://www.c-sharpcorner.com/UploadFile/8a67c0/C-Sharp-coding-standards-and-naming-conventions/)
Also the most important parts are documented, so it helps you understand how you can reuse them

## Some words about Interaction System

In our project, we use the custom-developed Interaction System. 
It allows us to make all Interactions physics-driven and give more control. 
We have started developing this on GGJ2020, so we can say that this system, already well-tested :)
Feel free to reuse and improve this in your own projects!

The most important things in our System are:
- Controller.cs
- Interactable.cs

Other behaviors are based on these two classes.

## In conclusion

We are glad to see any contribution. Want to improve the documentation or some part of code? 
No problem, just make PR to our repo. Have some bugs and do not know how to fix it? Fill issue,
and we will help you.