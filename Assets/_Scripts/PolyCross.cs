﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    public class PolyCross : MonoBehaviour
    {
        public Material PolyMaterial;
        public Material AdditionalPolyMaterial;
        public Transform Plane;

        void Update()
        {
            Plane plane = new Plane(Plane.forward, Plane.position);
            if(PolyMaterial != null)
                PolyMaterial.SetVector("_CrossPlane", new Vector4(plane.normal.x, plane.normal.y, plane.normal.z, plane.distance));
            if(AdditionalPolyMaterial != null)
                AdditionalPolyMaterial.SetVector("_CrossPlane", new Vector4(plane.normal.x, plane.normal.y, plane.normal.z, plane.distance));
        }
    }
}