﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    public class Blowtorch : MonoBehaviour
    {
        public ParticleSystem particleSystem;

        private Interactable _interactable;
        private GameObject _hovered;

        private void Start()
        {
            _interactable = GetComponent<Interactable>();
            _interactable.PrimaryDown += Mount;
            _interactable.PrimaryUp += StopParticles;

            particleSystem.Stop();
        }

        private void Mount()
        {
            particleSystem.Play();

            if (_hovered != null)
            {
                if (_hovered.GetComponent<Mount>().Mounted)
                {
                    _hovered.GetComponent<Mount>().Unhold();
                }
                else
                {
                    _hovered.GetComponent<Mount>().TryHold();
                }
            }
        }

        private void StopParticles()
        {
            particleSystem.Stop();
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Mount>() != null)
            {
                _hovered = other.gameObject;
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.gameObject == _hovered)
            {
                _hovered = null;
            }
        }
    }
}