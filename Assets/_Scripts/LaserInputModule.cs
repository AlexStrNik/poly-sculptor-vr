using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Valve.VR;

namespace PolySculptor
{
    public class LaserInputModule : BaseInputModule
    {
        public Camera PointerCamera;
        public SteamVR_Input_Sources Hand;
        public SteamVR_Action_Boolean InteractWithUI = SteamVR_Input.GetBooleanAction("InteractUI");

        private GameObject _currentObject;
        private PointerEventData _data;

        protected override void Awake()
        {
            base.Awake();

            _data = new PointerEventData(eventSystem);
        }

        public override void Process()
        {
            _data.Reset();
            _data.position = new Vector2(PointerCamera.pixelWidth / 2, PointerCamera.pixelHeight / 2);

            eventSystem.RaycastAll(_data, m_RaycastResultCache);
            _data.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
            _currentObject = _data.pointerCurrentRaycast.gameObject;

            m_RaycastResultCache.Clear();

            HandlePointerExitAndEnter(_data, _currentObject);

            if (InteractWithUI.GetStateDown(Hand))
                ProcessPress(_data);

            if (InteractWithUI.GetStateUp(Hand))
                ProcessRelease(_data);
        }

        private void ProcessPress(PointerEventData data)
        {
            data.pointerPressRaycast = data.pointerCurrentRaycast;

            data.pointerPress = ExecuteEvents.GetEventHandler<IPointerClickHandler>(data.pointerPressRaycast.gameObject);

            ExecuteEvents.Execute(data.pointerPress, data, ExecuteEvents.pointerDownHandler);
        }

        private void ProcessRelease(PointerEventData data)
        {
            GameObject pointerRelease = ExecuteEvents.GetEventHandler<IPointerClickHandler>(data.pointerCurrentRaycast.gameObject);

            if (data.pointerPress == pointerRelease)
                ExecuteEvents.Execute(data.pointerPress, data, ExecuteEvents.pointerClickHandler);

            ExecuteEvents.Execute(data.pointerPress, data, ExecuteEvents.pointerUpHandler);

            data.pointerPress = null;

            data.pointerCurrentRaycast.Clear();

            eventSystem.SetSelectedGameObject(null);

            data.pressPosition = Vector2.zero;
            data.pointerPress = null;
            data.rawPointerPress = null;
        }
    }
}