using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    /// <summary>
    /// Chisel tool. Can slice only stone parts.
    /// </summary>
    public class Chisel : MonoBehaviour
    {
        public Collider BladeCollider;

        private bool _locked = false;
        private GameObject _hovered;
        private GameObject _connected;
        private Interactable _interactable;
        private float _counter;
        private PolyCross _polyCross;

        private void Start()
        {
            _interactable = GetComponent<Interactable>();
            _interactable.Connected += Connected;
            _interactable.PrimaryDown += PrimaryDown;

            _polyCross = GetComponent<PolyCross>();
        }

        private void Unlock()
        {
            Physics.IgnoreCollision(BladeCollider, _connected.GetComponent<Collider>(), false);

            this.GetComponent<Rigidbody>().isKinematic = false;
            this.transform.parent = null;

            _connected = null;
            _locked = false;
            _counter = 0;
        }

        private void Connected()
        {
            if (_locked && _connected != null)
            {
                Unlock();
            }
        }

        private void PrimaryDown()
        {
            if (!_locked && _hovered != null)
            {
                _locked = true;
                _connected = _hovered;
                _hovered = null;

                this.GetComponent<Rigidbody>().isKinematic = true;
                this.transform.parent = _connected.transform;

                Physics.IgnoreCollision(BladeCollider, _connected.GetComponent<Collider>(), true);
                return;
            }

            if (_locked && _connected != null)
            {
                Unlock();
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<StoneSliceable>() != null)
            {
                _hovered = other.gameObject;
                var material = _hovered.GetComponent<MeshRenderer>().material;
                material.SetFloat("_CrossThickness", 1);
                _polyCross.PolyMaterial = material;

                if (_hovered.GetComponent<MeshRenderer>().materials.Length > 1) {
                    var additionalMaterial = _hovered.GetComponent<MeshRenderer>().materials[1];
                    _polyCross.AdditionalPolyMaterial = additionalMaterial;
                    additionalMaterial.SetFloat("_CrossThickness", 1);
                }
            }
        }
        void OnTriggerExit(Collider other)
        {
            if (other.gameObject == _hovered)
            {
                var material = _hovered.GetComponent<MeshRenderer>().material;
                material.SetFloat("_CrossThickness", 0);

                if (_hovered.GetComponent<MeshRenderer>().materials.Length > 1) {
                    var additionalMaterial = _hovered.GetComponent<MeshRenderer>().materials[1];
                    additionalMaterial.SetFloat("_CrossThickness", 0);
                }

                _polyCross.PolyMaterial = null;
                _polyCross.AdditionalPolyMaterial = null;
                _hovered = null;
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            if (_locked)
            {
                var velocity = collision.relativeVelocity.magnitude;
                this.transform.position += this.transform.forward * velocity / 15;
                _counter += velocity;

                if(_counter >= 2f) {
                    _connected.GetComponent<StoneSliceable>().Slice(this.transform.position, this.transform.up);
                    Unlock();
                }
            }
        }
    }
}