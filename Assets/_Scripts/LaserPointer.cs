using UnityEngine;
using Valve.VR;

namespace PolySculptor
{
    public class LaserPointer : MonoBehaviour
    {
        public SteamVR_Input_Sources Hand;
        public SteamVR_Action_Boolean InteractWithUI = SteamVR_Input.GetBooleanAction("InteractUI");
        public LayerMask LayerMask;
        public Color Color;
        public float Thickness = 0.002f;
        public Color ClickColor = Color.green;

        private GameObject _holder;
        private GameObject _pointer;

        private void Start()
        {
            _holder = new GameObject();
            _holder.transform.parent = this.transform;
            _holder.transform.localPosition = Vector3.zero;
            _holder.transform.localRotation = Quaternion.identity;

            _pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
            _pointer.transform.parent = _holder.transform;
            _pointer.transform.localScale = new Vector3(Thickness, Thickness, 100f);
            _pointer.transform.localPosition = new Vector3(0f, 0f, 50f);
            _pointer.transform.localRotation = Quaternion.identity;

            BoxCollider collider = _pointer.GetComponent<BoxCollider>();
            if (collider)
            {
                Object.Destroy(collider);
            }

            Material newMaterial = new Material(Shader.Find("Unlit/Color"));
            newMaterial.SetColor("_Color", Color);
            _pointer.GetComponent<MeshRenderer>().material = newMaterial;
        }

        private void Update()
        {
            float dist = 100f;
            Ray raycast = new Ray(transform.position, transform.forward);
            RaycastHit hit;

            bool bHit = Physics.Raycast(raycast, out hit, LayerMask);

            if (bHit && hit.distance < 100f)
            {
                dist = hit.distance;
            }

            if (InteractWithUI != null && InteractWithUI.GetState(Hand))
            {
                _pointer.transform.localScale = new Vector3(Thickness * 5f, Thickness * 5f, dist);
                _pointer.GetComponent<MeshRenderer>().material.color = ClickColor;
            }
            else
            {
                _pointer.transform.localScale = new Vector3(Thickness, Thickness, dist);
                _pointer.GetComponent<MeshRenderer>().material.color = Color;
            }

            _pointer.transform.localPosition = new Vector3(0f, 0f, dist / 2f);
        }
    }
}