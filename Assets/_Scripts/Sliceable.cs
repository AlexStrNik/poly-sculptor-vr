using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    public enum SliceableType
    {
        Wood,
        Stone,
    }

    /// <summary>
    /// Base class for all Sliceable objects.
    /// </summary>
    public class Sliceable : MonoBehaviour
    {
        public Material SliceMaterial;

        private void Start()
        {
            Material old = GetComponent<MeshRenderer>().material;
            GetComponent<MeshRenderer>().material = new Material(old);
        }

        public void Slice(Vector3 anchor, Vector3 normal)
        {
            SliceableType type = GetComponent<Freeze>().SliceableType;
            Material sliceMaterial = GetComponent<Freeze>().SliceMaterial;
            Material material = GetComponent<MeshRenderer>().material;
            material.SetFloat("_CrossThickness", 0);
            sliceMaterial.SetFloat("_CrossThickness", 0);

            float mass = GetComponent<Rigidbody>() != null ? GetComponent<Rigidbody>().mass : 1F;
            GameObject[] pieces = BLINDED_AM_ME.MeshCut.Cut(this.gameObject, anchor, normal, SliceMaterial);

            foreach (var piece in pieces)
            {
                if (piece.GetComponent<Collider>() != null)
                {
                    Destroy(piece.GetComponent<Collider>());
                }

                if(piece.GetComponent<Interactable>() == null)
                {
                    piece.AddComponent<Interactable>();
                }

                if(piece.GetComponent<Freeze>() == null)
                {
                    var freeze = piece.AddComponent<Freeze>();
                    freeze.SliceableType = type;
                    freeze.SliceMaterial = new Material(sliceMaterial);
                }

                if (piece.GetComponent<Mount>() == null)
                {
                    piece.AddComponent<Mount>();  
                }

                if(piece.GetComponent<Rigidbody>() == null)
                {
                    piece.AddComponent<Rigidbody>();
                }

                var collider = piece.AddComponent<MeshCollider>();
                collider.convex = true;

                piece.GetComponent<MeshRenderer>().material = new Material(material);
            }

            StartCoroutine(PolyPhysics.CalculateCutPhysicsAsync(pieces[0], pieces[1], mass));
        }
    }
}