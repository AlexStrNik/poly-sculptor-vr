﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "PolySculptor/WoodShader"
{
    Properties
    {
        _CirclesProfile ("Circles Profile", 2D) = "white" {}
        _BarkTexture ("Bark Texture", 2D) = "white" {}
        _BarkThickness("Bark Thickness", float) = 0.03
        _CrossLineColor ("Cross Line Color", Color) = (1, 0, 0, 0)
        _StencilMask("Stencil Mask", Range(0, 255)) = 255
        _CrossThickness("Cross Thickness", Range(0, 1)) = 1
    }

    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 2000
        //Cull Back
        

        CGPROGRAM

        #pragma surface surf Standard fullforwardshadows vertex:vert
        #pragma target 3.0

        #include "PolyCross.cginc"

        sampler2D _CirclesProfile;
        sampler2D _BarkTexture;

        float _BarkThickness;

        float4 _CrossPlane;
        fixed4 _CrossLineColor;
        float _CrossThickness;

        struct Input {
            float3 localPos;
            float3 normal;
            float3 worldPos;
            float2 uv_MainTex;
        };

        void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input,o);
            o.localPos = v.vertex;
            o.normal = v.normal;
            o.worldPos = mul(unity_ObjectToWorld, v.vertex);
        }

        void surf (Input i, inout SurfaceOutputStandard o)
        {
            float2 pos = i.localPos.xz;
            pos *= 1 + _BarkThickness;
            pos += float2(0.5, 0.5);

            float4 color = tex2D(_CirclesProfile, pos);
            if (length(pos - float2(0.5, 0.5)) > 0.5)
                color.a = 0;
            
            o.Albedo = 0;
            o.Albedo += color.rgb * color.a;

            o.Albedo += tex2D(_BarkTexture, float2(i.localPos.y, asin(i.localPos.z))).rgb * (1 - color.a);
            o.Alpha = 1;

            fixed3 crossAlbedo = o.Albedo;
            float crossFactor = get_cross_factor(_CrossPlane, i.worldPos, i.normal);

            crossAlbedo  = lerp(crossAlbedo, _CrossLineColor, crossFactor);
            if (crossFactor < 0.1 && get_plane_distance_nonabs(_CrossPlane, i.worldPos) > 0) {
                //crossAlbedo = lerp(crossAlbedo, fixed3(0.3, 0.9, 0), 0.8);
            }
            else {
                crossAlbedo = lerp(crossAlbedo, fixed3(0.3, 0.9, 0), 0.8);
                //crossAlbedo = lerp(crossAlbedo, fixed3(0.4, 0.9, 0), 0.23);
            }

            o.Albedo = lerp(o.Albedo, crossAlbedo, _CrossThickness);
            o.Alpha = 1;
        }
        ENDCG

            
    }
    FallBack "Diffuse"
}
